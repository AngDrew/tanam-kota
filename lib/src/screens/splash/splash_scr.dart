import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/providers/cross_screen_image_provider.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/utils/image_compress.dart';
//import 'package:flutter_image_compress/flutter_image_compress.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      prepareLandingImage(context);
    });
    Future<void>.delayed(const Duration(seconds: 5), () {
      Navigator.pushReplacementNamed<void, void>(context, '/landing');
    });
  }

  Future<void> prepareLandingImage(BuildContext context) async {
    print('loading image');

    final List<int> image =
        await ImageCompress.testCompressAsset('assets/images/logo.png');

    print('adding image to provider');

    Provider.of<CrossScreenImageProvider>(
      context,
      listen: false,
    ).landingImage = MemoryImage(
      Uint8List.fromList(image),
    );
  }

  final AssetImage image = const AssetImage('assets/images/logo.png');

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
            Spacing.vertical30,
            const Text(
              'Loading...',
              style: TSS.bgPrime,
            ),
          ],
        ),
      ),
    );
  }
}
