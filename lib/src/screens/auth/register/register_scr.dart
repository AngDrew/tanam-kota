import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/providers/validators/confirm_password_validator.dart';
import 'package:tanam_kota/src/providers/validators/email_validator.dart';
import 'package:tanam_kota/src/providers/validators/password_validator.dart';
import 'package:tanam_kota/src/providers/validators/phone_validator.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/widgets/neomorphism.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController pwdController = TextEditingController();
  TextEditingController cPwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          width: size.width,
          padding: const EdgeInsets.all(10),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // imageWidget(size, context),
              mainFormField(size),
              joinNowButton(size),
            ],
          ),
        ),
      ),
    );
  }

  Widget mainFormField(Size size) {
    return Container(
      width: size.width * 0.8,
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: emailForm(),
            ),
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: phoneForm(),
            ),
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: nameForm(),
            ),
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: usernameForm(),
            ),
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: pwdForm(),
            ),
            Spacing.vertical30,
            MakeNeomorphism(
              withHorizontalPadding: 25,
              withVerticalPadding: 1.5,
              child: cPwdForm(),
            ),
            Spacing.vertical30,
          ],
        ),
      ),
    );
  }

  Widget emailForm() {
    return Consumer<EmailValidator>(
      builder: (BuildContext context, EmailValidator validator, _) => TextField(
        controller: emailController,
        keyboardType: TextInputType.emailAddress,
        onChanged: validator.validateEmail,
        onEditingComplete: FocusScope.of(context).nextFocus,
        decoration: InputDecoration(
          // icon: const Icon(Icons.email),
          border: InputBorder.none,
          labelText: 'Email',
          hintText: 'Masukkan email anda',
          errorText: validator.onError,
          contentPadding: const EdgeInsets.all(0),
        ),
      ),
    );
  }

  Widget phoneForm() {
    return Consumer<PhoneValidator>(
      builder: (BuildContext context, PhoneValidator validator, _) => TextField(
        controller: phoneController,
        keyboardType: TextInputType.number,
        onChanged: validator.validatePhone,
        onEditingComplete: FocusScope.of(context).nextFocus,
        decoration: InputDecoration(
          // icon: const Icon(Icons.phone),
          border: InputBorder.none,
          labelText: 'Phone',
          hintText: 'Masukkan nomor telpon anda',
          errorText: validator.onError,
          contentPadding: const EdgeInsets.all(0),
        ),
      ),
    );
  }

  Widget nameForm() {
    return TextField(
      controller: nameController,
      onEditingComplete: FocusScope.of(context).nextFocus,
      decoration: const InputDecoration(
        // icon: Icon(Icons.person),
        border: InputBorder.none,
        labelText: 'Name',
        hintText: 'Masukkan nama anda',
        contentPadding: EdgeInsets.all(0),
      ),
    );
  }

  Widget usernameForm() {
    return TextField(
      controller: usernameController,
      onEditingComplete: FocusScope.of(context).nextFocus,
      decoration: const InputDecoration(
        // icon: Icon(Icons.contact_mail),
        border: InputBorder.none,
        labelText: 'Username',
        hintText: 'Masukkan username anda',
        contentPadding: EdgeInsets.all(0),
      ),
    );
  }

  FocusNode pwdFocus = FocusNode();

  Widget pwdForm() {
    pwdFocus.addListener(() {});
    return Consumer<PasswordValidator>(
      builder: (BuildContext context, PasswordValidator validator, _) => Column(
        children: <Widget>[
          TextField(
            controller: pwdController,
            focusNode: pwdFocus,
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            onChanged: validator.validatePassword,
            onEditingComplete: () {
              validator.onWarn.clear();
              FocusScope.of(context).unfocus();
              // FocusScope.of(context).nextFocus();
            },
            decoration: InputDecoration(
              // icon: const Icon(Icons.vpn_key),
              border: InputBorder.none,
              labelText: 'Password',
              hintText: 'Masukkan password anda',
              errorText: validator.onError,
              // suffixIconConstraints: const BoxConstraints(
              //   maxHeight: 25,
              //   maxWidth: 55,
              // ),
              suffixIcon: Container(
                constraints: const BoxConstraints(
                  maxWidth: 20,
                  maxHeight: 20,
                ),
                alignment: Alignment.center,
                child: pwdFocus.hasFocus
                    ? Stack(
                        children: <Widget>[
                          pwdController.text.isNotEmpty
                              ? passwordStrengthIndicator(validator)
                              : Container(),
                          pwdController.text.isNotEmpty
                              ? Container(
                                  width: 20,
                                  height: 20,
                                  child: GestureDetector(
                                    child: Icon(
                                      Icons.close,
                                      size: 20,
                                      color: Colors.grey,
                                    ),
                                    onTap: () {
                                      pwdController.text = '';
                                    },
                                  ),
                                )
                              : Container(
                                  width: 20,
                                  height: 20,
                                ),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      )
                    : Container(
                        width: 20,
                        height: 20,
                      ),
              ),
              contentPadding: const EdgeInsets.all(0),
            ),
          ),
        ],
      ),
    );
  }

  Container passwordStrengthIndicator(PasswordValidator validator) {
    return Container(
      width: 20,
      height: 20,
      child: CircularProgressIndicator(
        value: validator.strength,
        backgroundColor: Colors.transparent,
        strokeWidth: 3,
        valueColor: validator.strength == 0.25
            ? AlwaysStoppedAnimation<Color>(Colors.red)
            : validator.strength == 0.5
                ? AlwaysStoppedAnimation<Color>(Colors.orange)
                : validator.strength == 0.75
                    ? AlwaysStoppedAnimation<Color>(Colors.yellow)
                    : AlwaysStoppedAnimation<Color>(Colors.green),
      ),
    );
  }

  Widget pswStrengthMeter(PasswordValidator validator) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Wrap(
            children: List<Widget>.generate(
              validator.onWarn.length,
              (int index) => Row(
                children: <Widget>[
                  Icon(
                    Icons.warning,
                    color: Colors.yellowAccent[700],
                  ),
                  Text(
                    ' ${validator.onWarn.elementAt(index)}',
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget cPwdForm() {
    return Consumer<ConfirmPasswordValidator>(
      builder: (BuildContext context, ConfirmPasswordValidator validator, _) =>
          TextField(
        controller: cPwdController,
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        onChanged: (String value) {
          validator.validateConfirmPassword(
            value,
            pwdController.text,
          );
        },
        onEditingComplete: FocusScope.of(context).unfocus,
        decoration: InputDecoration(
          // icon: const Icon(Icons.vpn_key),
          border: InputBorder.none,
          labelText: 'Confirm Password',
          hintText: 'Masukkan konfirmasi password',
          errorText: validator.onError,
          contentPadding: const EdgeInsets.all(0),
        ),
      ),
    );
  }

  Container imageWidget(Size size, BuildContext context) {
    return Container(
      width: size.width * 0.5,
      child: Image(
        image:
            // Provider.of<CrossScreenImageProvider>(context).image,
            Image.asset('assets/images/logo.png').image,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.transparent,
      ),
    );
  }

  Widget joinNowButton(Size size) {
    return GestureDetector(
      onTap: buttonJoinPressed,
      child: MakeNeomorphism(
        withColor: ColorPalette.primary,
        child: Container(
          width: size.width * 0.8,
          height: 35,
          alignment: Alignment.center,
          child: const Text(
            'Join now',
            style: TSS.bgWhiteBold,
          ),
        ),
      ),
    );
  }

  void buttonJoinPressed() {
    if (true) {
      Navigator.pushNamedAndRemoveUntil(
          context, '/main', (Route<dynamic> route) => false);
    }
  }
}
