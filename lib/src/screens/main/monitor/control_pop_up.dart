import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/models/control_model.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/screens/main/monitor/monitor_vm.dart';
import 'package:tanam_kota/src/widgets/neomorphism.dart';

class ControlPopUp extends StatefulWidget {
  const ControlPopUp({
    this.index,
  });
  final int index;
  @override
  _ControlPopUpState createState() => _ControlPopUpState();
}

class _ControlPopUpState extends State<ControlPopUp> {
  @override
  void initState() {
    super.initState();
    //TODO: fetch data of the censor
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height * 0.1,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
        color: Colors.green[50],
      ),
      child: Consumer<MonitoringViewModel>(
          builder: (BuildContext context, MonitoringViewModel vm, _) {
        final ControlModel controller = vm.getTowers[widget.index].controlModel;

        final bool lamp = controller.growthLamp;
        final bool pump = controller.waterPump;
        // final bool refiller = controller.waterRefiller;

        const Widget _spacingBox = SizedBox(
          width: 10,
          height: 10,
        );
        return Wrap(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                vm.switchLamp(widget.index, !lamp);
              },
              child: MakeNeomorphism(
                withColor: Colors.greenAccent,
                withDisabledColor: Colors.redAccent,
                isDisabled: !lamp,
                withShadowColor: Colors.green[700],
                // withShineColor: Colors.white24,
                child: vm.isLampPressed
                    ? Container(
                        width: 16,
                        height: 16,
                        child: const CircularProgressIndicator(),
                      )
                    : Text(
                        'Growth Lamp',
                        style: TSS.md,
                      ),
              ),
              // Chip(
              //   label: Text(
              //     'Growth Lamp',
              //     style: TSS.md,
              //   ),
              //   backgroundColor: lamp ? Colors.greenAccent : Colors.redAccent,
              // ),
            ),
            _spacingBox,
            GestureDetector(
              onTap: () {
                vm.switchPump(widget.index, !pump);
              },
              child: MakeNeomorphism(
                withColor: Colors.greenAccent,
                withDisabledColor: Colors.redAccent,
                isDisabled: !pump,
                withShadowColor: Colors.green[700],
                // withShineColor: Colors.white24,
                child: vm.isPumpPressed
                    ? Container(
                        width: 16,
                        height: 16,
                        child: const CircularProgressIndicator(),
                      )
                    : Text(
                        'Water Pump',
                        style: TSS.md,
                      ),
              ),
            ),
            _spacingBox,
            // GestureDetector(
            //   onTap: () {
            //     vm.switchRefill(widget.index, !refiller);
            //   },
            //   child: Chip(
            //     label: Text(
            //       'Water Refiller',
            //       style: TSS.md,
            //     ),
            //     backgroundColor:
            //         refiller ? Colors.greenAccent : Colors.redAccent,
            //   ),
            // ),
            // _spacingBox,
          ],
        );
      }),
    );
  }
}
