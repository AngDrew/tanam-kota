import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:tanam_kota/src/models/sensor_model.dart';
import 'package:tanam_kota/src/models/control_model.dart';
import 'package:tanam_kota/src/models/my_chart_data.dart';
import 'package:tanam_kota/src/models/tower_model.dart';

class MonitoringViewModel extends ChangeNotifier {
  final List<TowerModel> _towers = <TowerModel>[];
  final List<SensorModel> _sensors = <SensorModel>[];

  List<TowerModel> get getTowers => _towers;
  List<SensorModel> get getSensors => _sensors;

  List<MyChartData> get chartDataAirTemp => _chartDataAirTemp;
  final List<MyChartData> _chartDataAirTemp = <MyChartData>[];

  List<MyChartData> get chartDataWaterTemp => _chartDataWaterTemp;
  final List<MyChartData> _chartDataWaterTemp = <MyChartData>[];

  List<MyChartData> get chartDataPhLevel => _chartDataPhLevel;
  final List<MyChartData> _chartDataPhLevel = <MyChartData>[];

  List<MyChartData> get chartDataPpm => _chartDataPpm;
  final List<MyChartData> _chartDataPpm = <MyChartData>[];

  List<MyChartData> get chartDataHumidity => _chartDataHumidity;
  final List<MyChartData> _chartDataHumidity = <MyChartData>[];

//firebase
  DatabaseReference userRef;

  void addSensorsFromJson(List<dynamic> rawDatas) {
    for (final dynamic item in rawDatas) {
      _sensors.add(SensorModel.fromJson(item as Map<String, dynamic>));
    }

    for (int i = 0; i < _sensors.length; i++) {
      _chartDataAirTemp.add(
        MyChartData(
          identifier: i,
          value:
              _sensors[i].airTemp == '' ? 0 : double.parse(_sensors[i].airTemp),
        ),
      );
      _chartDataPhLevel.add(
        MyChartData(
          identifier: i,
          value:
              _sensors[i].phLevel == '' ? 0 : double.parse(_sensors[i].phLevel),
        ),
      );
      _chartDataHumidity.add(
        MyChartData(
          identifier: i,
          value: _sensors[i].humidity == ''
              ? 0
              : double.parse(_sensors[i].humidity),
        ),
      );
      _chartDataWaterTemp.add(
        MyChartData(
          identifier: i,
          value: _sensors[i].waterTemp == ''
              ? 0
              : double.parse(_sensors[i].waterTemp),
        ),
      );
      _chartDataPpm.add(
        MyChartData(
          identifier: i,
          value: _sensors[i].ppm == '' ? 0 : double.parse(_sensors[i].ppm),
        ),
      );
    }

    notifyListeners();
  }

  void triggerTowerSensor(int index) {
    _towers[index].showSensor = !_towers[index].showSensor;
    notifyListeners();
  }

  void addTower(ControlModel controller) {
    _towers.add(TowerModel(
      sensorModel: getSensors,
      controlModel: controller,
    ));
    notifyListeners();
  }

  // void addCensors(CensorModel censor) {
  //   _sensors.add(censor);
  //   notifyListeners();
  // }
  // void addControllerStatus(ControlModel controller) {
  //   _controllerStatus.add(controller);
  //   notifyListeners();
  // }
  bool isPumpPressed = false;
  bool isLampPressed = false;

  void switchLamp(int index, bool newValue) async {
    isLampPressed = true;
    notifyListeners();
    await userRef.update(
      <String, bool>{'lamp': newValue},
    ).then((void value) => notifyListeners());
    getTowers[index].controlModel.growthLamp = newValue;
    isLampPressed = false;
    notifyListeners();
    //TODO: method to change the backend
  }

  void switchPump(int index, bool newValue) async {
    isPumpPressed = true;
    notifyListeners();
    await userRef.update(
      <String, bool>{'pump': newValue},
    ).then((void value) => notifyListeners());
    getTowers[index].controlModel.waterPump = newValue;
    isPumpPressed = false;
    notifyListeners();

    //TODO: method to change the backend
  }

  // void switchRefill(int index, bool newValue) {
  //   getTowers[index].controlModel.waterRefiller = newValue;
  //   notifyListeners();
  //   //TODO: method to change the backend
  // }

  /// `censorIndex` indicate the censor's separation
  ///
  /// 0 -> water level
  ///
  /// 1 -> ph
  ///
  /// 2 -> temperature
  ///
  /// 3 -> ppm
  double getValueRate(double value, int censorIndex) {
    if (censorIndex == 0) {
      return value / 30;
    } else if (censorIndex == 1) {
      return value / 12;
    } else if (censorIndex == 2) {
      return value / 50;
    } else if (censorIndex == 3) {
      return value / 100;
    }

    return 0;
  }
}
