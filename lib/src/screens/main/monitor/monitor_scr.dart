import 'package:charts_flutter/flutter.dart' as chart;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/models/my_chart_data.dart';
import 'package:tanam_kota/src/models/sensor_model.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/screens/main/main_vm.dart';
import 'package:tanam_kota/src/screens/main/monitor/control_pop_up.dart';
import 'package:tanam_kota/src/screens/main/monitor/monitor_vm.dart';
import 'package:tanam_kota/src/widgets/neomorphism.dart';

class MonitorScreen extends StatefulWidget {
  MonitorScreen(this.size);
  Size size;
  @override
  _MonitorScreenState createState() => _MonitorScreenState();
}

class _MonitorScreenState extends State<MonitorScreen> {
  bool addingNewPlant = false;
  MainViewModel mvm;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    mvm = Provider.of<MainViewModel>(context);
    return SingleChildScrollView(
      child: Container(
        margin: Spacing.all10,
        child: Column(
          children: <Widget>[
            Spacing.vertical20,
            Container(
              child: Consumer<MonitoringViewModel>(
                builder: (BuildContext context, MonitoringViewModel vm, _) {
                  return vm != null
                      ? mvm.isLoading
                          ? Container(
                              width: 30,
                              height: 30,
                              margin: const EdgeInsets.all(10),
                              child: const CircularProgressIndicator(),
                            )
                          : Column(
                              children: List<Widget>.generate(
                                vm.getTowers.length,
                                (int index) => plantWidget(
                                  index,
                                  vm.getTowers[index].sensorModel[0],
                                  vm.getTowers[index].showSensor,
                                ),
                              ),
                            )
                      : Container();
                },
              ),
            ),
            addTowerButton(),
          ],
        ),
      ),
    );
  }

  MakeNeomorphism addTowerButton() {
    return MakeNeomorphism(
      // withColor: ColorPalette.secondary,
      withHorizontalMargin: 10,
      withVerticalMargin: 10,
      child: Container(
        height: 50,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.add_circle_outline),
            const Text(
              'Add new tower',
              style: TSS.bgBlack,
            ),
          ],
        ),
      ),
    );
  }

  Container plantWidget(int index, SensorModel sensor, bool isShowingCensor) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          plantWidgetHeader(index, sensor),
          Spacing.vertical10,
          Center(
            child: Wrap(
              children: <Widget>[
                censorWidget('Ph', sensor.phLevel, unit: 'pH'),
                censorWidget('Humidity', sensor.humidity, unit: 'g/m3'),
                censorWidget('PPM', sensor.ppm),
                censorWidget('Air Temp', sensor.airTemp, unit: '°C'),
                censorWidget('Water Temp', sensor.waterTemp, unit: '°C'),
              ],
            ),
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 100),
            height: (isShowingCensor) ? 1400 : 0,
            width: MediaQuery.of(context).size.width,
            child: isShowingCensor
                ? Consumer<MonitoringViewModel>(
                    builder:
                        (BuildContext context, MonitoringViewModel vm, _) =>
                            FutureBuilder<dynamic>(
                      future: Future<dynamic>.delayed(
                        const Duration(milliseconds: 100),
                      ).then<Widget>(
                        (dynamic _) => censorGraph(),
                      ),
                      builder: (
                        BuildContext context,
                        AsyncSnapshot<dynamic> snapshot,
                      ) =>
                          AnimatedOpacity(
                        duration: const Duration(milliseconds: 1000),
                        opacity: snapshot.data == null ? 0 : 1,
                        child: snapshot.data == null
                            ? Container()
                            : snapshot.data as Widget,
                      ),
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
  }

  Row plantWidgetHeader(int index, SensorModel sensor) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            'Tower $index',
            style: TSS.bgBlack,
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Consumer<MonitoringViewModel>(
                  builder: (BuildContext context, MonitoringViewModel vm, _) =>
                      GestureDetector(
                    child: MakeNeomorphism(
                      // withColor: Colors.lightGreen[50],
                      withShape: BoxShape.circle,
                      isDisabled: vm.getTowers[index].showSensor,
                      withDisabledColor: Colors.lightGreen[50],
                      child: Icon(
                        Icons.trending_up,
                        size: 30,
                      ),
                    ),
                    onTap: () {
                      vm.triggerTowerSensor(index);
                    },
                  ),
                ),
                Spacing.horizontal10,
                GestureDetector(
                  onTap: () => viewCensorControl(index, context),
                  child: MakeNeomorphism(
                    // withColor: Colors.lightGreen[50],
                    withShape: BoxShape.circle,
                    child: Icon(
                      Icons.settings,
                      size: 30,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void viewCensorControl(int index, BuildContext context) {
    // TODO: open BottomSheet
    //how?!
    showBottomSheet<dynamic>(
      context: context,
      builder: (BuildContext bc) => ControlPopUp(
        index: index,
      ),
    );
  }

  Widget censorWidget(String censorName, String value, {String unit = ''}) {
    return MakeNeomorphism(
      // withColor: Colors.lightGreen[50],
      withHorizontalMargin: 10,
      withVerticalMargin: 5,
      withHorizontalPadding: 10,
      withVerticalPadding: 0,
      child: Container(
        // width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10),
        // alignment: Alignment.center,
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.end,
          children: <Widget>[
            Text(
              '$censorName',
              style: TSS.bgBlack,
            ),
            Text(
              ' $value',
              style: TSS.mdBlack,
            ),
            Container(
              child: Text(
                '$unit',
                style: TSS.smBlack,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// this is the graph widget
  Widget censorGraph() {
    return Consumer<MonitoringViewModel>(
      builder: (BuildContext context, MonitoringViewModel vm, _) => Column(
        children: <Widget>[
          //
          Spacing.vertical20,
          censorGraphWidget(
            idName: 'pH Level',
            censorsData: vm.chartDataPhLevel,
          ),
          //
          Spacing.vertical20,
          censorGraphWidget(
            idName: 'Humidity',
            censorsData: vm.chartDataHumidity,
          ),
          //
          Spacing.vertical20,
          censorGraphWidget(
            idName: 'PPM',
            censorsData: vm.chartDataPpm,
          ),
          //
          Spacing.vertical20,
          censorGraphWidget(
            idName: 'Air Temp',
            censorsData: vm.chartDataAirTemp,
          ),
          //
          Spacing.vertical20,
          censorGraphWidget(
            idName: 'Water Temp',
            censorsData: vm.chartDataWaterTemp,
          ),
        ],
      ),
    );
  }

  Widget censorGraphWidget({String idName, List<MyChartData> censorsData}) {
    return Column(
      children: <Widget>[
        MakeNeomorphism(
          withColor: Colors.lightGreen[50],
          withVerticalPadding: 10,
          withHorizontalPadding: 10,
          withHorizontalMargin: 10,
          withVerticalMargin: 10,
          child: Container(
            height: 200,
            child: chart.LineChart(
              <chart.Series<MyChartData, int>>[
                chart.Series<MyChartData, int>(
                  id: idName,
                  data: censorsData,
                  domainFn: (MyChartData datum, _) => datum.identifier, //x axis
                  measureFn: (MyChartData datum, _) => datum.value, //y axis
                  colorFn: (MyChartData datum, _) => datum.color,
                ),
              ],
              animate: true,
            ),
          ),
        ),
        Text(idName, style: TSS.mdBlack),
      ],
    );
  }
}
