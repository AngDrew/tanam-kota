import 'package:dio/dio.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/models/control_model.dart';
import 'package:tanam_kota/src/resources/constants.dart';
import 'package:tanam_kota/src/resources/nav_tab.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/screens/main/commodity/commodity_scr.dart';
import 'package:tanam_kota/src/screens/main/main_vm.dart';
import 'package:tanam_kota/src/screens/main/monitor/monitor_scr.dart';
import 'package:tanam_kota/src/utils/fetch_util.dart';
import 'package:tanam_kota/src/widgets/my_drawer.dart';
import 'package:tanam_kota/src/widgets/responsive_safe_area.dart';

import 'home/home_scr.dart';
import 'monitor/monitor_vm.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback(
      // TODO: this is fetch simulation
      (_) async {
        MonitoringViewModel vm;
        MainViewModel mvm;

        vm = Provider.of<MonitoringViewModel>(context, listen: false);
        mvm = Provider.of<MainViewModel>(context, listen: false);

        // get the censor from first page (recent censor data)
        fetch = FetchUtil('${Constants.baseUrl}/sensor/1');

        print('getting data from the server');

        mvm.setLoadingStatus(true);

        final Response<dynamic> data = await fetch.getData();

        vm.userRef = FirebaseDatabase.instance
            .reference()
            .child('hycomate-1/005Ld3Ril4NCjaetyAlM');
        // vm.userRef = FirebaseDatabase.instance
        //     .reference()
        //     .child('{userId}/{towerId}');

        // vm.userRef.child('censor').set(value)

        final List<dynamic> finalData = data.data as List<dynamic>;

        vm.addSensorsFromJson(finalData);

        // add censor to tower
        vm.addTower(
          ControlModel.fromJson(
            await vm.userRef.once().then<Map<String, bool>>(
                  (DataSnapshot value) => Map<String, bool>.from(
                      value.value as Map<dynamic, dynamic>),
                ),
          ),
        );

        mvm.setLoadingStatus(false);
      },
    );
  }

  FetchUtil fetch;
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  void openDrawers() {
    _drawerKey.currentState.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      drawer: MyDrawer(),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: ResponsiveSafeArea(
        builder: (BuildContext context, Size size) => contentBody(size),
        // Stack(
        //   children: <Widget>[
        //     contentBody(size),
        //     //making custom appbar
        //     // drawerButton(),
        //   ],
        // ),
      ),
      bottomNavigationBar: bottomNavBar(),
    );
  }

  Consumer<MainViewModel> contentBody(Size size) {
    return Consumer<MainViewModel>(
      builder: (BuildContext context, MainViewModel vm, _) => IndexedStack(
        index: vm.index,
        children: vm.tabs.map<Widget>((NavTab tab) {
          switch (vm.index) {
            case 0:
              return HomeScreen(size);
            case 1:
              return CommodityScreen(size);
            case 2:
              return MonitorScreen(size);
            default:
              return const Center(
                child: Text('Screen does not exist'),
              );
          }
        }).toList(),
      ),
    );
  }

  Consumer<MainViewModel> bottomNavBar() {
    return Consumer<MainViewModel>(
      builder: (BuildContext context, MainViewModel vm, _) {
        return BottomNavigationBar(
          showUnselectedLabels: false,
          showSelectedLabels: true,
          currentIndex: vm.index,
          onTap: (int index) => vm.moveToScreen(index),
          items: vm.tabs
              .map(
                (NavTab tab) => BottomNavigationBarItem(
                  icon: Icon(
                    tab.icon,
                    size: 28,
                  ),
                  title: Text(
                    '•', //•
                    style: TSS.bg,
                  ),
                ),
              )
              .toList(),
        );
      },
    );
  }

  Positioned drawerButton() {
    return Positioned(
      top: 10,
      left: 10,
      child: GestureDetector(
        onTap: openDrawers,
        child: Icon(
          Icons.menu,
          size: 35,
        ),
      ),
    );
  }
}
