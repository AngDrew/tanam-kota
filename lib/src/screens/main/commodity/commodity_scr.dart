import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/widgets/neomorphism.dart';

class CommodityScreen extends StatefulWidget {
  const CommodityScreen(this.size);
  final Size size;
  @override
  _CommodityScreenState createState() => _CommodityScreenState();
}

class _CommodityScreenState extends State<CommodityScreen> {
  int _index = 0;
  PageController pageController = PageController(viewportFraction: 0.75);

  @override
  void initState() {
    super.initState();
    pageController.addListener(() {});
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: List<Widget>.generate(
          10,
          (int index) => Container(
            width: widget.size.width,
            margin: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                header(),
                //
                footer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container header() {
    return Container(
      width: widget.size.width,
      height: widget.size.height * 0.25,
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: Image.network(
          'https://hellosehat.com/wp-content/uploads/2016/11/sayuran-hidroponik.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Container footer() {
    return Container(
      padding: Spacing.all5,
      child: Column(
        children: <Widget>[
          Text(
            'Dino Sayurus',
            style: TSS.lgBlackBold,
          ),
          Spacing.vertical10,
          timeLabel(),
          //
          durationLabel(),
          Spacing.vertical10,
          Container(
            width: widget.size.width,
            // alignment: Alignment.topRight,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: MakeNeomorphism(
                    withVerticalPadding: 10,
                    withHorizontalPadding: 20,
                    withHorizontalMargin: 5,
                    withColor: Colors.green[50],
                    child: const Center(
                      child: Text(
                        'Info',
                        style: TSS.mdBlackBold,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: MakeNeomorphism(
                    withVerticalPadding: 10,
                    withHorizontalPadding: 20,
                    withHorizontalMargin: 5,
                    withColor: Colors.green[50],
                    child: const Center(
                      child: Text(
                        'Schedule Plant',
                        style: TSS.mdBlackBold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Row timeLabel() {
    return Row(
      //price
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            'Price: ',
            style: TSS.bgBlack,
          ),
        ),
        Expanded(
          flex: 2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                'Rp. ' '10.000',
                style: TSS.bgBlack,
              ),
              Text(
                ' / Kg',
                style: TSS.smBlack,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Row durationLabel() {
    return Row(
      // duration
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            'Duration: ',
            style: TSS.bgBlack,
          ),
        ),
        Expanded(
          flex: 2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                '30',
                style: TSS.bgBlack,
              ),
              Text(
                ' Days',
                style: TSS.smBlack,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
