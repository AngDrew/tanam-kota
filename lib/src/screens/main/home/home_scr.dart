import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../resources/spacing.dart';
import '../../../resources/spacing.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen(this.size);
  final Size size;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text(
                  'Produk',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                Text(
                  'Selengkapnya',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.green,
                  ),
                ),
              ],
            ),
            Spacing.vertical20,
            StaggeredGridView.countBuilder(
              crossAxisCount: 3,
              shrinkWrap: true,
              crossAxisSpacing: 8,
              staggeredTileBuilder: (int index) => const StaggeredTile.fit(3),
              itemBuilder: (BuildContext context, int i) => Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                ),
                padding: const EdgeInsets.only(bottom: 16),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                width: widget.size.width / 3,
                // height: 400,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.network(
                      'https://dummyimage.com/600x400/000/fff',
                      height: 100,
                      fit: BoxFit.cover,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                        horizontal: 4,
                        vertical: 4,
                      ),
                      child: const Text(
                        'Product Name',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.green,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 4),
                      child: const Text('Rp. 1.000.000'),
                    ),
                    Spacing.vertical10,
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 4),
                      child: const Text(
                        'good for something that makes you very happy when'
                        ' you can do something with them',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
