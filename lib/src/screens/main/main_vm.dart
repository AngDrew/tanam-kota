import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tanam_kota/src/resources/icons/my_icons_icons.dart';
import 'package:tanam_kota/src/resources/nav_tab.dart';

class MainViewModel extends ChangeNotifier {
  int _index = 0;

  bool isLoading = false;

  List<NavTab> tabs = <NavTab>[
    NavTab(title: 'Home', icon: Icons.home, color: Colors.green),
    NavTab(title: 'Commodity', icon: Icons.work, color: Colors.green),
    NavTab(title: 'Monitor', icon: MyIcons.plant_and_root, color: Colors.green),
    // NavTab(title: 'Supply', icon: Icons.shopping_cart, color: Colors.green),
  ];

  int get index => _index;

  void moveToScreen(int to) {
    _index = to;
    notifyListeners();
  }

  void setLoadingStatus(bool value) {
    isLoading = value;
    notifyListeners();
  }
}
