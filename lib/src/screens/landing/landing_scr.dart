import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/providers/cross_screen_image_provider.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';
import 'package:tanam_kota/src/widgets/neomorphism.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() {
    super.initState();
  }

  ImageProvider imgProvider;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          height: size.height,
          padding: const EdgeInsets.all(10),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Expanded(
                flex: 3,
                child: SizedBox(),
              ),
              //circle logo [image]
              imageWidget(size, context),
              const Expanded(
                flex: 1,
                child: SizedBox(),
              ),
              //welcome to the next chapter of urban-farming
              labelText(),
              const Expanded(
                flex: 6,
                child: SizedBox(),
              ),
              //join now [button]
              joinNowButton(size),
              Spacing.vertical20,
              //sign in [button],
              signInButton(size),
              Spacing.vertical20,
            ],
          ),
        ),
      ),
    );
  }

  Container imageWidget(Size size, BuildContext context) {
    return Container(
      width: size.width * 0.5,
      child: Image(
        image: Provider.of<CrossScreenImageProvider>(context).landingImage,
        // Image.asset('assets/images/logo.png').image,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.transparent,
      ),
    );
  }

  Container labelText() {
    return Container(
      child: Text(
        'Welcome\nto the next chapter\nof urban-farming',
        style: TSS.xlBlack,
      ),
    );
  }

  Widget joinNowButton(Size size) {
    return GestureDetector(
      onTap: buttonJoinPressed,
      child: MakeNeomorphism(
        withColor: ColorPalette.primary,
        child: Container(
          width: size.width * 0.8,
          height: 35,
          alignment: Alignment.center,
          child: const Text(
            'Join now',
            style: TSS.bgWhiteBold,
          ),
        ),
      ),
    );
  }

  Widget signInButton(Size size) {
    return GestureDetector(
      onTap: buttonSignInPressed,
      child: MakeNeomorphism(
        isPressed: isSignInPressed,
        child: Container(
          width: size.width * 0.8,
          height: 35,
          alignment: Alignment.center,
          child: Text(
            'Sign in',
            style: TSS.bgPrimeBold,
          ),
        ),
      ),
    );
  }

  void buttonJoinPressed() {
    if (true) {
      Navigator.pushNamed<void>(context, '/register');
    }
  }

  void buttonSignInPressed() {
    if (true) {
      setState(() {
        isSignInPressed = !isSignInPressed;
      });
      //TODO: adding method for pressed button
    }
  }

  bool isSignInPressed = false;
}
