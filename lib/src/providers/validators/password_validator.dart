import 'package:flutter/foundation.dart';

class PasswordValidator extends ChangeNotifier {
  String _error;
  final Set<String> _warn = <String>[].toSet();

  String get onError => _error;
  double get strength => (4 - _warn.length) / 4;

  Set<String> get onWarn => _warn;

  String validatePassword(String value) {
    final bool isValid =
        RegExp(r'^[a-zA-Z0-9\ \(\)\[\]\{\}\*\+\?.\^\|\/\.\\\!\@\#\$\%\^\&\_\=]')
                .hasMatch(value) &&
            value.length >= 4 &&
            value.length <= 32;
    //
    final RegExp aCapital = RegExp('[A-Z]');
    final RegExp aNumber = RegExp('[0-9]');
    final RegExp aSymbol =
        RegExp(r'[\ \(\)\[\]\{\}\*\+\?.\^\|\/\.\\\!\@\#\$\%\^\&\_\=]');

    if (!value.contains(aCapital)) {
      _warn.add('No capital character');
    } else {
      _warn.remove('No capital character');
    }

    if (!value.contains(aNumber)) {
      _warn.add('Not contains number');
    } else {
      _warn.remove('Not contains number');
    }

    if (value.length < 8) {
      _warn.add('Less than 8 digits');
    } else {
      _warn.remove('Less than 8 digits');
    }

    if (!value.contains(aSymbol)) {
      _warn.add('Not contains symbol character');
    } else {
      _warn.remove('Not contains symbol character');
    }

    if (!isValid) {
      _error = 'Password must be 4-32 digits!';
      notifyListeners();
      return value;
    }

    _error = null;
    notifyListeners();
    return value;
  }
}
