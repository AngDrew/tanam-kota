import 'package:flutter/foundation.dart';

class EmailValidator extends ChangeNotifier {
  String _error;

  String get onError => _error;

  String validateEmail(String value) {
    final bool isValid = RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value);
    if (!isValid) {
      _error = 'Invalid email!';
      notifyListeners();
      return value;
    }
    _error = null;
    notifyListeners();
    return value;
  }
}
