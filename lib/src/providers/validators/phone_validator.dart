import 'package:flutter/foundation.dart';

class PhoneValidator extends ChangeNotifier {
  String _error;

  String get onError => _error;

  String validatePhone(String value) {
    final bool isValid = RegExp(r'^(?:0)?[0-9]$').hasMatch(value);
    if (!isValid && value.length < 10) {
      _error = 'Invalid phone number!';
      notifyListeners();
      return value;
    }
    _error = null;
    notifyListeners();
    return value;
  }
}
