import 'package:flutter/foundation.dart';

class ConfirmPasswordValidator extends ChangeNotifier {
  String _error;

  String get onError => _error;

  String validateConfirmPassword(String value, String _password) {
    if (value != _password) {
      _error = 'Confirm password does not match!';
      notifyListeners();
      return value;
    }
    _error = null;
    notifyListeners();
    return value;
  }
}
