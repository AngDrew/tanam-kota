import 'package:flutter/material.dart';

class NavTab {
  NavTab({
    this.title,
    this.icon,
    this.color,
  });
  final String title;
  final IconData icon;
  final MaterialColor color;
}
