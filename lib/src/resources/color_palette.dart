import 'package:flutter/material.dart';

class ColorPalette {
  // https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=66BB6A&secondary.color=CCFF90

  static const Color primary = Color(0xff66bb6a);
  static const Color primaryLight = Color(0xff98ee99);
  static const Color primaryDark = Color(0xff338a3e);

  static const Color secondary = Color(0xffccff90);
  static const Color secondaryLight = Color(0xffffffc2);
  static const Color secondaryDark = Color(0xff99cc60);

  static const Color textColor = Color(0xff000000);

  static const Color greyColored = Color(0xffE1E2E1);
  static const Color backgroundColor = Color(0xffF5F5F6);
}
