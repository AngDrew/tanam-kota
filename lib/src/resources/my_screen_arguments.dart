import 'package:tanam_kota/src/models/sensor_model.dart';

class MyScreenArguments {
  MyScreenArguments({
    this.censor,
  });

  /// i wanted to pass the censor data to another screen that's why i need
  /// this arguments and what else do i need to pass here? is it just censor
  /// or how about another thing?
  final SensorModel censor;
}
