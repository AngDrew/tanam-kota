/// Flutter icons MyIcons
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  MyIcons
///      fonts:
///       - asset: fonts/MyIcons.ttf
///
///
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
/// * Entypo, Copyright (C) 2012 by Daniel Bruce
///         Author:    Daniel Bruce
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.entypo.com
///
import 'package:flutter/widgets.dart';

class MyIcons {
  MyIcons._();

  static const String _kFontFam = 'MyIcons';

  static const IconData plant_with_leaves =
      IconData(0xe800, fontFamily: _kFontFam);
  static const IconData all_inclusive = IconData(0xe801, fontFamily: _kFontFam);
  static const IconData android = IconData(0xe802, fontFamily: _kFontFam);
  static const IconData done_all = IconData(0xe803, fontFamily: _kFontFam);
  static const IconData done = IconData(0xe804, fontFamily: _kFontFam);
  static const IconData fingerprint = IconData(0xe805, fontFamily: _kFontFam);
  static const IconData key = IconData(0xe806, fontFamily: _kFontFam);
  static const IconData plant_and_root =
      IconData(0xe807, fontFamily: _kFontFam);
}
