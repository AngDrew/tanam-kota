import 'package:flutter/material.dart';

class Spacing {
  //horizontal spacing
  static const SizedBox horizontal5 = SizedBox(
    width: 5,
  );
  static const SizedBox horizontal10 = SizedBox(
    width: 10,
  );
  static const SizedBox horizontal20 = SizedBox(
    width: 20,
  );
  //vertical spacing
  static const SizedBox vertical5 = SizedBox(
    height: 5,
  );
  static const SizedBox vertical10 = SizedBox(
    height: 10,
  );
  static const SizedBox vertical20 = SizedBox(
    height: 20,
  );
  static const SizedBox vertical30 = SizedBox(
    height: 30,
  );
  static const SizedBox vertical40 = SizedBox(
    height: 40,
  );
  static const SizedBox vertical50 = SizedBox(
    height: 50,
  );
  static const SizedBox vertical60 = SizedBox(
    height: 60,
  );
  static const SizedBox vertical70 = SizedBox(
    height: 70,
  );
  static const SizedBox vertical80 = SizedBox(
    height: 80,
  );
  static const SizedBox vertical90 = SizedBox(
    height: 90,
  );
  static const SizedBox vertical100 = SizedBox(
    height: 100,
  );

  //horizontal margin or padding
  static const EdgeInsets horizontalSymetric5 = EdgeInsets.symmetric(
    horizontal: 5,
  );
  static const EdgeInsets horizontalSymetric10 = EdgeInsets.symmetric(
    horizontal: 10,
  );
  static const EdgeInsets horizontalSymetric15 = EdgeInsets.symmetric(
    horizontal: 15,
  );
  static const EdgeInsets horizontalSymetric20 = EdgeInsets.symmetric(
    horizontal: 20,
  );
  static const EdgeInsets horizontalSymetric25 = EdgeInsets.symmetric(
    horizontal: 25,
  );
  static const EdgeInsets horizontalSymetric30 = EdgeInsets.symmetric(
    horizontal: 30,
  );
  static const EdgeInsets horizontalSymetric35 = EdgeInsets.symmetric(
    horizontal: 35,
  );
  static const EdgeInsets horizontalSymetric40 = EdgeInsets.symmetric(
    horizontal: 40,
  );
  static const EdgeInsets horizontalSymetric45 = EdgeInsets.symmetric(
    horizontal: 45,
  );
  static const EdgeInsets horizontalSymetric50 = EdgeInsets.symmetric(
    horizontal: 50,
  );
  // all padding
  static const EdgeInsets all5 = EdgeInsets.all(5);
  static const EdgeInsets all10 = EdgeInsets.all(10);
  static const EdgeInsets all15 = EdgeInsets.all(15);
  static const EdgeInsets all20 = EdgeInsets.all(20);
  static const EdgeInsets all25 = EdgeInsets.all(25);
  static const EdgeInsets all30 = EdgeInsets.all(30);
  static const EdgeInsets all35 = EdgeInsets.all(35);
  static const EdgeInsets all40 = EdgeInsets.all(40);
  static const EdgeInsets all45 = EdgeInsets.all(45);
  static const EdgeInsets all50 = EdgeInsets.all(50);

  static const EdgeInsets noSpace = EdgeInsets.all(0);
}
