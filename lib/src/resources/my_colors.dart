import 'package:flutter/material.dart';

class MyColors {
  static const MaterialColor white = MaterialColor(
    0xFFFFFFFF,
    <int, Color>{
      50: Color(0xFFFFFFFF),
      100: Color(0xFFFAFAFA),
      200: Color(0xFFF5F5F5),
      300: Color(0xFFF0F0F0),
      400: Color(0xFFDEDEDE),
      500: Color(0xFFC2C2C2),
      600: Color(0xFF979797),
      700: Color(0xFF818181),
      800: Color(0xFF606060),
      900: Color(0xFF3C3C3C),
    },
  );

  static const MaterialColor black = MaterialColor(
    0xFF000000,
    <int, Color>{
      50: Color(0xFFF5F5F5),
      100: Color(0xFFE9E9E9),
      200: Color(0xFFD9D9D9),
      300: Color(0xFFC4C4C4),
      400: Color(0xFF9D9D9D),
      500: Color(0xFF7B7B7B),
      600: Color(0xFF555555),
      700: Color(0xFF434343),
      800: Color(0xFF262626),
      900: Color(0xFF000000),
    },
  );
}
