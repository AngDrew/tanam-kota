import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/providers/validators/confirm_password_validator.dart';
import 'package:tanam_kota/src/providers/validators/email_validator.dart';
import 'package:tanam_kota/src/providers/validators/password_validator.dart';
import 'package:tanam_kota/src/providers/validators/phone_validator.dart';
import 'package:tanam_kota/src/screens/auth/register/register_scr.dart';
import 'package:tanam_kota/src/screens/landing/landing_scr.dart';
import 'package:tanam_kota/src/screens/main/main_scr.dart';
import 'package:tanam_kota/src/screens/main/main_vm.dart';
import 'package:tanam_kota/src/screens/main/monitor/monitor_scr.dart';
import 'package:tanam_kota/src/screens/main/monitor/monitor_vm.dart';
import 'package:tanam_kota/src/screens/splash/splash_scr.dart';
import 'package:tanam_kota/src/utils/screen_transition/fade_route.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return FadeRoute(page: SplashScreen());
      // return FadeRoute(page: MonitoringScreen());
      case '/landing':
        return FadeRoute(page: LandingScreen());
//      case '/login':
//        return FadeRoute(page: LoginScreen());
      case '/register':
        return FadeRoute(
          page: MultiProvider(
            providers: <SingleChildCloneableWidget>[
              ChangeNotifierProvider<EmailValidator>.value(
                  value: EmailValidator()),
              ChangeNotifierProvider<PhoneValidator>.value(
                  value: PhoneValidator()),
              ChangeNotifierProvider<PasswordValidator>.value(
                  value: PasswordValidator()),
              ChangeNotifierProvider<ConfirmPasswordValidator>.value(
                  value: ConfirmPasswordValidator()),
            ],
            child: RegisterScreen(),
          ),
        );
      case '/main':
        return FadeRoute(
          page: MultiProvider(
            providers: <SingleChildCloneableWidget>[
              ChangeNotifierProvider<MainViewModel>.value(
                value: MainViewModel(),
              ),
              ChangeNotifierProvider<MonitoringViewModel>.value(
                value: MonitoringViewModel(),
              ),
            ],
            child: MainScreen(),
          ),
          // page: ChangeNotifierProvider<MainViewModel>.value(
          //   value: MainViewModel(),
          //   child: MainScreen(),
          // ),
        );
//      case '/main':
//        return FadeRoute(page: MainScreen());
//      case '/offer':
//        return FadeRoute(
//            page: OfferListScreen(), settings: settings);
//      case '/garden':
//        return FadeRoute(
//            page: GardenListScreen(), settings: settings);
//      case '/commodity':
//        return FadeRoute(
//            page: CommodityScreen(), settings: settings);
      default:
        return FadeRoute(
          page: Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
