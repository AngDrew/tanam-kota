class ControlModel {
  //status on/off
  //censor name
  ControlModel({
    this.growthLamp,
    this.waterPump,
  });

  ControlModel.fromJson(Map<String, bool> json)
      : waterPump = json['pump'] ?? false,
        growthLamp = json['lamp'] ?? false;

  bool waterPump;
  bool growthLamp;
}
