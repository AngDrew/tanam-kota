import 'package:flutter/foundation.dart';

class TimeModel {
  TimeModel({
    @required this.sec,
    @required this.nanosec,
  });
  final int sec;
  final int nanosec;
}
