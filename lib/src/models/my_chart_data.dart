import 'package:charts_flutter/flutter.dart' as chart;

class MyChartData {
  MyChartData({
    this.color = const chart.Color(
      r: 0,
      g: 0,
      b: 0,
      a: 255,
    ),
    this.identifier = 0,
    this.value = 0,
  });
  //the x of the line chart
  int identifier; //x
  /// the Y of the line chart
  double value; //y
  chart.Color color;
}
