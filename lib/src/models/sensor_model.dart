import 'package:tanam_kota/src/models/time_model.dart';

class SensorModel {
  SensorModel(
    this.phLevel,
    this.humidity,
    this.waterTemp,
    this.airTemp,
    this.ppm,
    this._rawTime,
  );

  SensorModel.fromJson(Map<String, dynamic> json)
      : phLevel = json['data_ph'] as String,
        humidity = json['data_humidity'] as String,
        waterTemp = json['data_water_temp'] as String,
        airTemp = json['data_air_temp'] as String,
        ppm = json['data_ppm'] as String,
        _rawTime = TimeModel(
          sec: json['waktu']['seconds'] as int,
          nanosec: json['waktu']['nanoseconds'] as int,
        );

  DateTime getTime() {
    return DateTime.fromMicrosecondsSinceEpoch(
      ((_rawTime.sec * 1000000) + (_rawTime.nanosec / 1000)).floor(),
    );
  }

  final String phLevel;
  final String humidity;
  final String waterTemp;
  final String airTemp;
  final String ppm;
  final TimeModel _rawTime;
  // sec -> micro(* 10^6) + nano -> micro(/ 10^3)
}
