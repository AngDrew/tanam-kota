class UserModel {
  UserModel({
    this.name,
    this.birthDay,
    this.phoneNumber,
    this.photoUrl,
  });

  final String name;
  final String photoUrl;
  final String phoneNumber;
  final DateTime birthDay;
}
