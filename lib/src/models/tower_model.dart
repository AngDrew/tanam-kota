import 'package:tanam_kota/src/models/sensor_model.dart';
import 'package:tanam_kota/src/models/control_model.dart';

class TowerModel {
  TowerModel({
    this.sensorModel,
    this.controlModel,
  });
  final ControlModel controlModel;
  final List<SensorModel> sensorModel;
  bool showSensor = false;
}
