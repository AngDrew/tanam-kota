import 'package:flutter/material.dart';
import 'package:tanam_kota/src/resources/spacing.dart';
import 'package:tanam_kota/src/resources/text_style_sheet.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacing.vertical50,
          CircleAvatar(
            maxRadius: 75,
            backgroundColor: Colors.green,
            child: Container(),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: Spacing.horizontalSymetric10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Nama',
                  style: TSS.xlBlack,
                ),
                //
                //
                //
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                // Icon(
                //   Icons.settings,
                //   size: 36,
                // ),
                // Spacing.horizontal20,
                const Icon(
                  Icons.exit_to_app,
                  size: 36,
                ),
                Spacing.horizontal10,
              ],
            ),
          ),
          Spacing.vertical10,
          // const Text('This is the Drawer'),
          // RaisedButton(
          //   onPressed: _closeDrawer,
          //   child: const Text('Close Drawer'),
          // ),
        ],
      ),
    );
  }
}
