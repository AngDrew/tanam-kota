import 'package:flutter/material.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';
import 'package:tanam_kota/src/resources/my_colors.dart';

class MakeNeomorphism extends StatelessWidget {
  const MakeNeomorphism({
    Key key,
    @required this.child,
    this.withShape = BoxShape.rectangle,
    this.withHorizontalPadding = 5,
    this.withVerticalPadding = 5,
    this.withHorizontalMargin = 0,
    this.withVerticalMargin = 0,
    this.withColor = ColorPalette.backgroundColor,
    this.withDisabledColor,
    this.withRadiusBorder = 50,
    this.isPressed = false,
    this.childWhenPressed,
    this.isDisabled = false,
    this.withShineColor = MyColors.white,
    this.withShadowColor = Colors.black,
  }) : super(key: key);

  /// child of the Container() will be shown in the middle of the
  /// object frame
  final Widget child;

  /// used to add padding vertically (top and bottom)
  final double withVerticalPadding;

  /// used to add padding horizontally (right and left)
  final double withHorizontalPadding;

  /// used to add margin vertically (top and bottom)
  final double withVerticalMargin;

  /// used to add margin horizontally (right and left)
  final double withHorizontalMargin;

  /// preferable background color, by default using the background color from
  /// the color palette
  final Color withColor;

  /// preferable background color when button is disabled, by default using the background color from
  /// the color palette
  final Color withDisabledColor;

  /// determind the shape of the object
  final BoxShape withShape;

  /// is applied when the object shape is Rectangle, will be
  /// ignored if the object shape is circle
  final double withRadiusBorder;

  /// makes the button look disabled by removing it's shadow
  final bool isDisabled;

  /// indicating if the button is pressed or not
  /// to show a different object by removing the shadow and can be combined
  /// with `childWhenPressed` to show a different child when pressed
  final bool isPressed;

  /// child when the pressed is true
  ///
  /// when isPressed = true and this widget null, then
  /// child widget will be shown
  final Widget childWhenPressed;

  final Color withShineColor;

  final Color withShadowColor;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 100),
      padding: EdgeInsets.only(
        top: withVerticalPadding,
        bottom: withVerticalPadding,
        left: withHorizontalPadding,
        right: withHorizontalPadding,
      ),
      margin: EdgeInsets.only(
        top: withVerticalMargin,
        bottom: withVerticalMargin,
        left: withHorizontalMargin,
        right: withHorizontalMargin,
      ),
      decoration: BoxDecoration(
        color: isPressed || isDisabled
            ? withDisabledColor ?? MyColors.white[900].withOpacity(0.1)
            : withColor,
        boxShadow: isPressed || isDisabled
            ? <BoxShadow>[
                BoxShadow(
                  offset: const Offset(-1, -1),
                  color: withShadowColor.withOpacity(0.2),
                  blurRadius: 0,
                  spreadRadius: 1,
                ),
                BoxShadow(
                  offset: const Offset(1, 1),
                  color: withShineColor,
                  blurRadius: 0,
                  spreadRadius: 1,
                ),
              ]
            : <BoxShadow>[
                BoxShadow(
                  offset: const Offset(-3, -3),
                  color: withShineColor,
                  blurRadius: 1.5,
                  spreadRadius: 0.5,
                ),
                BoxShadow(
                  offset: const Offset(3, 3),
                  color: withShadowColor.withOpacity(0.2),
                  blurRadius: 3,
                  spreadRadius: 0,
                ),
              ],
        shape: withShape,
        borderRadius: (withShape != BoxShape.rectangle)
            ? null
            : BorderRadius.circular(withRadiusBorder),
      ),
      child: isPressed
          ? Stack(
              alignment: Alignment.center,
              children: <Widget>[
                childWhenPressed ?? child,
                const CircularProgressIndicator(),
              ],
            )
          : child,
    );
  }
}
