import 'package:dio/dio.dart';

class FetchUtil {
  FetchUtil(
    this.url,
  );
  Dio dio = Dio();
  final String url;

  Future<Response<dynamic>> getData() {
    return dio.get<dynamic>(url);
  }
}
