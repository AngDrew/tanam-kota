import 'dart:io';

import 'package:flutter_image_compress/flutter_image_compress.dart';

class ImageCompress {
  // 1. compress file and get a List<int>
  static Future<List<int>> testCompressFile(File file) async {
    final List<int> result = await FlutterImageCompress.compressWithFile(
      file.absolute.path,
      quality: 100,
    );
    print(file.lengthSync());
    print(result.length);
    return result;
  }

  // 2. compress file and get file.
  static Future<File> testCompressAndGetFile(
      File file, String targetPath) async {
    final File result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 100,
    );

    print(file.lengthSync());
    print(result.lengthSync());

    return result;
  }

  // 3. compress asset and get List<int>.
  static Future<List<int>> testCompressAsset(String assetName) async {
    return await FlutterImageCompress.compressAssetImage(
      assetName,
      quality: 100,
      format: CompressFormat.webp,
    );
  }

  // 4. compress List<int> and get another List<int>.
  static Future<List<int>> testComporessList(List<int> list) async {
    final List<int> result = await FlutterImageCompress.compressWithList(
      list,
      quality: 100,
    );
    print(list.length);
    print(result.length);
    return result;
  }
}
