import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tanam_kota/src/resources/color_palette.dart';

import 'src/providers/cross_screen_image_provider.dart';
import 'src/resources/router.dart' as router;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildCloneableWidget>[
        // ChangeNotifierProvider<CrossScreenImageProvider>.value(
        //   value: CrossScreenImageProvider(),
        // ),
        ChangeNotifierProvider<CrossScreenImageProvider>(
          create: (_) => CrossScreenImageProvider(),
        )
      ],
      child: MaterialApp(
        title: 'Tanam Kota',
        theme: ThemeData(
          primarySwatch: Colors.green,
          primaryColor: ColorPalette.primary,
          primaryColorLight: ColorPalette.primaryLight,
          primaryColorDark: ColorPalette.primaryDark,
          accentColor: ColorPalette.secondary,
          scaffoldBackgroundColor: ColorPalette.backgroundColor,
          bottomSheetTheme: const BottomSheetThemeData(
            backgroundColor: Colors.transparent,
          ),
        ),
        onGenerateRoute: router.Router.generateRoute,
        initialRoute: '/',
      ),
    );
  }
}
