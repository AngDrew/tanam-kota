# tanam_kota

tanam kota hydroponic

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## How to run the code
- you have installed flutter SDK, android SDK, etc. you can see [all of them by clicking here](https://flutter.dev/docs/get-started/install)
- plug your phone into the pc/laptop, and enable Developer Options, and then enable USB Debugging on Developer Options
- open the repo using VSCode/Android Studio/IntelliJ with installed flutter plugin
- and then run `flutter pub get` to get the dependencies
- finally run `flutter run --release` command to start the app